<?php

return
    [
        'class'         => 'yii\rest\UrlRule',
        'controller'    => [
            'catalog/material-group',
        ],
        'tokens'        => [
            '{id}' => '<id:\\d[\\d,]*>',
        ],
        'extraPatterns' => [
            'GET {id}/materials' => 'viewMaterialGroupMaterials',
        ],
        'pluralize'     => false,
    ];