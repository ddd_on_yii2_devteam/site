<?php

return
    [
        'class'         => 'yii\rest\UrlRule',
        'controller'    => [
            'catalog/option',
        ],
        'tokens'        => [
            '{id}'        => '<id:\\d+>',
            '{elementId}' => '<elementId:\\d+>',
        ],
        'extraPatterns' => [
            'GET {id}/property'                => 'viewProperty',
            'GET {id}/variants'                => 'viewVariants',

            'POST {id}/property/{elementId}'   => 'createRelationOption2Property',
            'DELETE {id}/property/{elementId}' => 'deleteRelationOption2Property',
        ],
        'pluralize'     => false,
    ];