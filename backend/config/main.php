<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

$routes = require(__DIR__ . '/../../backend/config/routes.php');

return [
    'id'                  => 'app-backend',
    'basePath'            => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap'           => [
        'log',
        'media',
    ],
    'modules'             => [
        'crm'     => [
            'class' => \backend\modules\crm\Module::class,
        ],
        'catalog' => [
            'class' => \backend\modules\catalog\Module::class,
        ],
        'core'    => [
            'class' => \backend\modules\core\Module::class,
        ],
        'billing' => [
            'class' => \backend\modules\billing\Module::class,
        ],
        'media'   => [
            'class'           => commonprj\modules\media\Module::class,
            'mediaServiceUrl' => 'http://media-service.furniprice.local/image',
            'recognizeColors' => 10,
            'maxSize'         => 1024 * 1024 * 2,
            'extensions'      => 'gif, png, jpg',
        ],
    ],
    'components'          => [
        'request'      => [
            'csrfParam' => '_csrf-backend',
            'parsers'   => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'response'     => [
            'format'  => yii\web\Response::FORMAT_JSON,
            'charset' => 'UTF-8',
        ],
        'user'         => [
            'identityClass'   => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie'  => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session'      => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager'   => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => array_merge([
                'GET /core/property/directories'                       => 'core/property-directories',
                'GET /catalog/material/directories'                    => 'catalog/material-directories',
                'GET /catalog/material-group/with-materials'           => 'catalog/material-group-with-materials',
                'GET /catalog/product-material/with-extended-grouping' => 'catalog/product-material-with-extended-grouping',
                'GET /crm/directories'                                 => 'crm/directories',
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => ['media/image'],
                    'tokens'        => [
                        '{id}' => '<id:[a-f0-9]+>',
                    ],
                    'extraPatterns' => [],
                    'pluralize'     => false,
                ],
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [
                        'core/property-directories',
                        'catalog/material-directories',
                        'catalog/material-group-with-materials',
                        'catalog/product-material-with-extended-grouping',
                    ],
                    'tokens'        => [],
                    'extraPatterns' => [],
                    'pluralize'     => false,
                ],
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [
                        'core/property-group',
                        'core/property-unit',
                        'core/property-type',
                    ],
                    'tokens'        => [
                        '{id}' => '<id:\\d[\\d,]*>',
                    ],
                    'extraPatterns' => [
                        'GET {id}/properties' => 'viewPropertyGroupOrPropertyUnitProperties',
                    ],
                    'pluralize'     => false,
                ],
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [
                        'core/element-class',
                    ],
                    'tokens'        => [
                        '{id}'         => '<id:\\d[\\d,]*>',
                        '{propertyId}' => '<propertyId:\\d[\\d,]*>',
                    ],
                    'extraPatterns' => [
                        'GET {id}/context'                  => 'viewElementClassContext',
                        'GET {id}/properties'               => 'viewElementClassProperties',
                        'GET {id}/relation-classes'         => 'viewElementClassRelationClasses',
                        'POST {id}/property/{propertyId}'   => 'createElementClass2Property',
                        'DELETE {id}/property/{propertyId}' => 'deleteElementClass2Property',
                    ],
                    'pluralize'     => false,
                ],
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [
                        'catalog/material-template',
                    ],
                    'tokens'        => [
                        '{id}' => '<id:\\d[\\d,]*>',
                    ],
                    'extraPatterns' => [
                        'GET {id}/materials' => 'viewMaterialTemplateMaterials',
                    ],
                    'pluralize'     => false,
                ],
                [
                    'class'      => 'yii\rest\UrlRule',
                    'controller' => [
                        'crm/user-session',
                    ],
                    'pluralize'  => false,
                ],
                [
                    //Получение переводов
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [
                        'catalog/product-material',
                        'catalog/price-category',
                        'catalog/material-collection',
                        'catalog/product-model',
                        'crm/seller',
                        'crm/manufacturer',
                        'core/property',
                    ],
                    'tokens'        => [
                        '{id}' => '<id:\\d[\\d,]*>',
                    ],
                    'extraPatterns' => [
                        'GET {id}/translation' => 'getTranslationData',
                    ],
                    'pluralize'     => false,

                ],
            ], $routes),
        ],
    ],
    'params'              => $params,
];
