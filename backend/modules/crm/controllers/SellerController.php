<?php

namespace backend\modules\crm\controllers;

use backend\controllers\BaseBackendController;
use common\extendedStdComponents\crm\seller as SellerAction;
use commonprj\components\crm\entities\seller\Seller;


/**
 * Class SellerController
 * @package api\controllers
 */
class SellerController extends BaseBackendController
{
    /**
     * @var string
     */
    public $modelClass = Seller::class;

    /**
     * @inheritdoc
     */
    protected function addActions(): array
    {
        return [
            'createRelationSeller2Manufacturer' => [
                'class'       => SellerAction\CreateRelationSeller2ManufacturerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationSeller2Manufacturer' => [
                'class'       => SellerAction\DeleteRelationSeller2ManufacturerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewManufacturers' => [
                'class'       => SellerAction\ViewManufacturersAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewProductRequests' => [
                'class'       => SellerAction\ViewProductRequestsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationSeller2Address' => [
                'class'       => SellerAction\CreateRelationSeller2AddressAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationSeller2Address' => [
                'class'       => SellerAction\DeleteRelationSeller2AddressAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewAddresses' => [
                'class'       => SellerAction\ViewAddressesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationSeller2CompanyRole' => [
                'class'       => SellerAction\CreateRelationSeller2CompanyRoleAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationSeller2CompanyRole' => [
                'class'       => SellerAction\DeleteRelationSeller2CompanyRoleAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewCompanyRole' => [
                'class'       => SellerAction\ViewCompanyRoleAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationSeller2CompanyState' => [
                'class'       => SellerAction\CreateRelationSeller2CompanyStateAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationSeller2CompanyState' => [
                'class'       => SellerAction\DeleteRelationSeller2CompanyStateAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewCompanyState' => [
                'class'       => SellerAction\ViewCompanyStateAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewEmployees' => [
                'class'       => SellerAction\ViewEmployeesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationSeller2Employee' => [
                'class'       => SellerAction\CreateRelationSeller2EmployeeAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationSeller2Employee' => [
                'class'       => SellerAction\DeleteRelationSeller2EmployeeAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

}