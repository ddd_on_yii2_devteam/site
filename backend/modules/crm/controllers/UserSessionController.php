<?php

namespace backend\modules\crm\controllers;

use commonprj\services\StoreService;
use Yii;
use yii\rest\Controller;

/**
 * Class AddressController
 * @package api\modules\crm\controllers
 */
class UserSessionController extends Controller
{
    public function actionIndex()
    {
        /** @var StoreService $storeService */
        $storeService = Yii::$app->storeService;

        return $storeService->getData();
    }

    public function actionCreate()
    {
        /** @var StoreService $storeService */

        $request = Yii::$app->getRequest();
        $storeService = Yii::$app->storeService;

        $storeService->setData($request->getBodyParams());

    }

}