<?php

namespace backend\modules\crm\controllers;

use commonprj\components\core\entities\property\Property;
use yii\rest\Controller;
use common\extendedStdComponents\crm\directories;

/**
 * Class AddressController
 * @package api\modules\crm\controllers
 */
class DirectoriesController extends Controller
{
    /**
     * @var string
     */
    public $modelClass = Property::class;

    public function actions(): array
    {
        return [
            'index' => [
                'class'       => directories\ViewDirectoriesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }
}