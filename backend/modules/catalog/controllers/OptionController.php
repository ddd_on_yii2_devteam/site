<?php

namespace backend\modules\catalog\controllers;

use backend\controllers\BaseBackendController;
use common\extendedStdComponents\catalog\option as OptionAction;
use commonprj\components\catalog\entities\option\Option;


/**
 * Class OptionController
 * @package api\controllers
 */
class OptionController extends BaseBackendController
{
    /**
     * @var string
     */
    public $modelClass = Option::class;

    /**
     * @inheritdoc
     */
    protected function addActions(): array
    {
        return [
            'create' => [
                'class'       => OptionAction\CreateOptionAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationOption2Property' => [
                'class'       => OptionAction\CreateRelationOption2PropertyAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationOption2Property' => [
                'class'       => OptionAction\DeleteRelationOption2PropertyAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewProperty' => [
                'class'       => OptionAction\ViewPropertyAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewVariants' => [
                'class'       => OptionAction\ViewVariantsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

}