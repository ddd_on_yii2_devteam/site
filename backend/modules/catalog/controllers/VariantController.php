<?php

namespace backend\modules\catalog\controllers;

use backend\controllers\BaseBackendController;
use common\extendedStdComponents\catalog\variant as VariantAction;
use commonprj\components\catalog\entities\variant\Variant;


/**
 * Class VariantController
 * @package api\controllers
 */
class VariantController extends BaseBackendController
{
    /**
     * @var string
     */
    public $modelClass = Variant::class;

    /**
     * @inheritdoc
     */
    protected function addActions(): array
    {
        return [
            'create' => [
                'class'       => VariantAction\CreateVariantAndBindOptionAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationVariant2Option' => [
                'class'       => VariantAction\CreateRelationVariant2OptionAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationVariant2Option' => [
                'class'       => VariantAction\DeleteRelationVariant2OptionAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewOption' => [
                'class'       => VariantAction\ViewOptionAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

}