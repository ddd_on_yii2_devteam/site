<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22.03.2018
 * Time: 13:55
 */

namespace backend\modules\catalog\controllers;

use backend\controllers\BaseBackendController;
use common\extendedStdComponents\catalog\material\ViewMaterialDirectoriesAction;
use commonprj\components\catalog\entities\material\Material;

/**
 * Class MaterialDirectories
 * @package backend\controllers
 */
class MaterialDirectoriesController extends BaseBackendController
{
    /**
     * @var string
     */
    public $modelClass = Material::class;

    /**
     * @return array
     */
    public function actions(): array
    {
        return [
            'index' => [
                'class'       => ViewMaterialDirectoriesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }
}