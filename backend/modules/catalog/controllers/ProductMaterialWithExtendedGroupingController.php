<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.04.2018
 * Time: 14:06
 */

namespace backend\modules\catalog\controllers;

use backend\controllers\BaseBackendController;
use common\extendedStdComponents\catalog\materialGroup\ViewMaterialGroupsWithMaterialsAction;
use commonprj\components\catalog\entities\productMaterial\ProductMaterial;

/**
 * Class ProductMaterialWithExtendedGrouping
 * @package backend\modules\catalog\controllers
 */
class ProductMaterialWithExtendedGroupingController extends BaseBackendController
{
    /**
     * @var string
     */
    public $modelClass = ProductMaterial::class;

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'index' => [
                'class'       => ViewMaterialGroupsWithMaterialsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];

    }
}