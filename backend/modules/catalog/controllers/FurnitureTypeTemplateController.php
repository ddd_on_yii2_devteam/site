<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 04.04.2018
 * Time: 18:38
 */

namespace backend\modules\catalog\controllers;

use backend\controllers\BaseBackendController;
use common\extendedStdComponents\catalog\furnitureTypeTemplate\CreateFurnitureTypeTemplateAction;
use common\extendedStdComponents\catalog\furnitureTypeTemplate\UpdateFurnitureTypeTemplateAction;
use commonprj\components\core\entities\template\Template;

class FurnitureTypeTemplateController extends BaseBackendController
{
    public $modelClass = Template::class;

    /**
     * @return array
     */
    public function actions(): array
    {
        $actions = parent::actions();

        $actions['create'] = [
            'class'       => CreateFurnitureTypeTemplateAction::class,
            'modelClass'  => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];

        $actions['update'] = [
            'class'       => UpdateFurnitureTypeTemplateAction::class,
            'modelClass'  => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];

        return $actions;
    }
}