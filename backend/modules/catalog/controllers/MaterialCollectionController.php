<?php

namespace backend\modules\catalog\controllers;

use backend\controllers\BaseBackendController;
use common\extendedStdComponents\catalog\materialCollection as MaterialCollectionAction;
use commonprj\components\catalog\entities\materialCollection\MaterialCollection;


/**
 * Class MaterialCollectionController
 * @package api\controllers
 */
class MaterialCollectionController extends BaseBackendController
{
    /**
     * @var string
     */
    public $modelClass = MaterialCollection::class;

    /**
     * @inheritdoc
     */
    protected function addActions(): array
    {
        return [
            'create' => [
                'class'       => MaterialCollectionAction\CreateMaterialCollectionAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationMaterialCollection2Manufacturer' => [
                'class'       => MaterialCollectionAction\CreateRelationMaterialCollection2ManufacturerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationMaterialCollection2Manufacturer' => [
                'class'       => MaterialCollectionAction\DeleteRelationMaterialCollection2ManufacturerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewManufacturer' => [
                'class'       => MaterialCollectionAction\ViewManufacturerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

}