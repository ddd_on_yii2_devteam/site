<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03.04.2018
 * Time: 18:49
 */

namespace backend\modules\catalog\controllers;

use backend\controllers\BaseBackendController;
use common\extendedStdComponents\catalog\furnitureType\CreateFurnitureTypeAction;
use common\extendedStdComponents\catalog\furnitureType\IndexFurnitureAction;
use common\extendedStdComponents\catalog\furnitureType\ViewTemplateAction;
use common\extendedStdComponents\UpdateAction;
use common\extendedStdComponents\ViewAction;
use commonprj\components\core\entities\PropertyTreeItem\PropertyTreeItem;

/**
 * Class FurnitureTypeController
 * @package backend\modules\catalog\controllers
 */
class FurnitureTypeController extends BaseBackendController
{
    /**
     * @var string
     */
    public $modelClass = PropertyTreeItem::class;

    /**
     * @return array
     */
    public function actions(): array
    {
        return [
            'create'       => [
                'class'       => CreateFurnitureTypeAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'index'        => [
                'class'       => IndexFurnitureAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'update'       => [
                'class'       => UpdateAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'view'         => [
                'class'       => ViewAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewTemplate' => [
                'class'       => ViewTemplateAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }
}