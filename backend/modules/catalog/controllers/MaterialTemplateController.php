<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28.02.2018
 * Time: 17:34
 */

namespace backend\modules\catalog\controllers;

use backend\controllers\BaseBackendController;
use common\extendedStdComponents\catalog\materialTemplate\ViewMaterialTemplateMaterialsAction;
use commonprj\components\catalog\entities\materialTemplate\MaterialTemplate;

/**
 * Class MaterialTemplateController
 * @package backend\controllers
 */
class MaterialTemplateController extends BaseBackendController
{
    /**
     * @var string
     */
    public $modelClass = MaterialTemplate::class;

    /**
     * @return array
     */
    public function addActions(): array
    {
        return [
            'viewMaterialTemplateMaterials' => [
                'class'       => ViewMaterialTemplateMaterialsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }
}