<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 14.02.2018
 * Time: 15:05
 */

namespace backend\modules\core\controllers;

use backend\controllers\BaseBackendController;
use common\extendedStdComponents\core\property\ViewPropertyDirectoriesAction;
use commonprj\components\core\entities\property\Property;

/**
 * Class PropertyDirectoriesController
 * @package backend\controllers
 */
class PropertyDirectoriesController extends BaseBackendController
{
    /**
     * @var string
     */
    public $modelClass = Property::class;

    /**
     * @return array
     */
    public function actions(): array
    {
        return [
            'index' => [
                'class'       => ViewPropertyDirectoriesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }
}