<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.02.2018
 * Time: 11:16
 */

namespace backend\modules\core\controllers;

use backend\controllers\BaseBackendController;
use common\extendedStdComponents\core\property\ViewPropertyGroupOrPropertyUnitPropertiesAction;
use commonprj\components\core\entities\propertyUnit\PropertyUnit;

/**
 * Class PropertyUnitController
 * @package backend\controllers
 */
class PropertyUnitController extends BaseBackendController
{
    /**
     * @var string
     */
    public $modelClass = PropertyUnit::class;

    /**
     * @return array
     */
    public function addActions(): array
    {
        return [
            'viewPropertyGroupOrPropertyUnitProperties' => [
                'class'       => ViewPropertyGroupOrPropertyUnitPropertiesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }
}