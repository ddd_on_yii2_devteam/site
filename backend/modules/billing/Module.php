<?php

namespace backend\modules\billing;

/**
 * Class Module
 * @package api\modules\core
 */
class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();
    }
}