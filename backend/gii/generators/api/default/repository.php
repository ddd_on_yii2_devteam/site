<?php
/* @var $generator \backend\gii\generators\api\Generator */

//dd($generator->methods);
echo '<?php' . PHP_EOL;
?>

namespace commonprj\components\<?= $generator->entityContext ?>\entities\<?= $generator->shortClassName ?>;

use commonprj\extendedStdComponents\BaseServiceRepository;
use Yii;
use yii\httpclient\Client;

/**
 * Class <?= $generator->shortClassName ?>ServiceRepository
 * @package commonprj\components\<?= $generator->entityContext ?>\entities\<?= $generator->shortClassName ?>
 */
class <?= $generator->shortClassName ?>ServiceRepository extends BaseServiceRepository
{
    /**
     * @var Client
     */
    protected $restServer;

    /**
     * @var string
     */
    protected $baseUri = '<?= \yii\helpers\Inflector::camel2id($generator->shortClassName) ?>/';

    /**
     * <?= $generator->shortClassName ?>ServiceRepository constructor.
     */
    public function __construct()
    {
        $this->restServer = Yii::$app->catalogManager;
    }

<?php foreach ($generator->methods as $method): ?>
    public function <?= $method['name'] ?>(<?= (!empty($method['allParams'])) ? implode(', ', $method['allParams']) : '' ?>)
    {
    <?php if('get' == $method['type']): ?>
        $this->requestUri = $this->getBaseUri($<?= lcfirst($generator->entityName) ?>->id . '/<?= $method['uri'] ?>');
        $this->requestParams = [];
        $content = $this->getApiData();

        if (!empty($content)){
        $result = $this->getOneModel($content, <?= preg_replace('|^get|', '', $method['name']) ?>::className());
        }

        return $result ?? null;
    <?php endif; ?>
    <?php if('bind' == $method['type']): ?>
        $uri = $this->getBaseUri($<?= lcfirst($generator->entityName) ?>->id . '/<?= $method['uri'] ?>/' . $<?= $method['params']['relation']['name'] ?>Id);
        $response = $this->restServer->post($uri)->send();
        return $response->getIsOk();
    <?php endif; ?>
    <?php if('unbind' == $method['type']): ?>
        $uri = $this->getBaseUri($<?= lcfirst($generator->entityName) ?>->id . '/<?= $method['uri'] ?>/' . $<?= $method['params']['relation']['name'] ?>Id);
        $response = $this->restServer->delete($uri)->send();
        return $response->getIsOk();
    <?php endif; ?>
    }

<?php endforeach; ?>
}