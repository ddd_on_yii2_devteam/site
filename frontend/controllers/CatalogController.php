<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 14.02.2018
 * Time: 17:05
 */

namespace frontend\controllers;

use Cloudinary;
use commonprj\components\catalog\entities\productModel\ProductModel;
use yii\web\Controller;
use Yii;
use yii\web\Response;

/**
 * Class CatalogController
 * @package frontend\controllers
 */
class CatalogController extends Controller
{
    /**
     * @return string
     * @throws \Exception
     * @throws \yii\web\HttpException
     */
    public function actionIndex()
    {
        $response = $this->actionProductModel(false);

        return $this->render('search.html.twig', [
            'products' => $response['items'],
            'filters'  => $response['filters'],
            'pager'    => $response['pager'],
        ]);
    }

    /**
     * @param bool $responseJson
     * @return array
     * @throws \Exception
     * @throws \yii\web\HttpException
     */
    public function actionProductModel($responseJson = true)
    {
        if ($responseJson) {
            Yii::$app->response->format = Response::FORMAT_JSON;
        }

        $attributes = Yii::$app->request->getQueryParams();
        $entity = new ProductModel();
        $response = $entity->findAll($attributes);
        $products = $response['items'] ?? [];

        foreach ((array)$products as $key => $product) {
            $product['properties'] = $this->indexPropertiesBySysname($product['properties']);

            $imageUrls = [];
            $images = $product['properties']['image']['value'] ?? [];

            foreach ($images as $image) {
                $imageUrls[] = cloudinary_url($image);
            }

            $product['properties'] = $product['properties'] + ["imageUrls" => $imageUrls];
            $products[$key] = $product;
        }

        $response['items'] = $products;
        return $response;
    }

    /**
     * @return array|\commonprj\extendedStdComponents\BaseCrudModel[]
     */
    public function actionProductModelFilter()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $attributes = Yii::$app->request->getQueryParams();

        $entity = new ProductModel();
        return $entity->repository->getFilters($attributes);
    }

    /**
     * @param array $properties
     * @return array
     */
    private function indexPropertiesBySysname(array $properties)
    {
        $result = [];

        foreach ($properties as $property) {
            $result[$property['sysname']] = $property;
        }

        return $result;
    }
}