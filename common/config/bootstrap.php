<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@commonprj', dirname(dirname(__DIR__)) . '/commonprj');

Cloudinary::config([
    'cloud_name'    => 'furniprice',
    'api_key'       => '',
    'api_secret'    => '',
    'cdn_subdomain' => 'true',
]);