<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 29.06.2016
 */

namespace common\extendedStdComponents;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\BaseInflector;
use yii\web\BadRequestHttpException;
use commonprj\components\core\helpers\ClassAndContextHelper;
use commonprj\extendedStdComponents\BaseCrudModel;
use commonprj\extendedStdComponents\BaseAction;
use commonprj\components\core\entities\element\Element;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class IndexAction extends BaseAction
{
    /**
     * @return static[]
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $queryParams = Yii::$app->getRequest()->getQueryParams();

        return $this->modelClass::findAll($queryParams);
    }
}
