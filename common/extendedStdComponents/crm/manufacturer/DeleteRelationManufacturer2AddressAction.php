<?php

namespace common\extendedStdComponents\crm\manufacturer;

use commonprj\components\crm\entities\manufacturer\Manufacturer;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class Manufacturer * @package api\controllers
 */
class DeleteRelationManufacturer2AddressAction extends BaseAction
{

    /**
     * @param int $id
     * @param int $elementId
     * @return array|bool
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id, int $elementId)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Manufacturer $entity
         */
        $entity = $this->findModel($id);

        return $entity->unbindAddress($elementId) ?? [];
    }

}