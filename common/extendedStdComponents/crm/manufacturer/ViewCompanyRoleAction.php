<?php

namespace common\extendedStdComponents\crm\manufacturer;

use commonprj\components\crm\entities\manufacturer\Manufacturer;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class Manufacturer * @package api\controllers
 */
class ViewCompanyRoleAction extends BaseAction
{

    /**
     * @param int $id
     * @return \commonprj\components\crm\entities\companyRole\CompanyRole
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id) 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Manufacturer $entity
         */
        $entity = $this->findModel($id);

        return $entity->getCompanyRole();
    }

}