<?php

namespace common\extendedStdComponents\crm\address;

use commonprj\components\crm\entities\address\Address;
use commonprj\components\crm\entities\customer\Customer;
use commonprj\components\crm\entities\employee\Employee;
use commonprj\components\crm\entities\manufacturer\Manufacturer;
use commonprj\components\crm\entities\seller\Seller;
use commonprj\extendedStdComponents\BaseAction;
use commonprj\extendedStdComponents\IBaseCrudModel;
use Yii;
use yii\web\HttpException;


/**
 * Class CreateAddressAndBindToOwnerAction
 * @package common\extendedStdComponents\address
 */
class CreateAddressAndBindToOwnerAction extends BaseAction
{
    private const OWNER_CLASSES = [
        'manufacturer' => Manufacturer::class,
        'seller'       => Seller::class,
        'employee'     => Employee::class,
        'customer'     => Customer::class,
    ];

    /**
     * @return mixed
     * @throws HttpException
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $ownerId = Yii::$app->getRequest()->getBodyParam('ownerId');
        $owner = Yii::$app->getRequest()->getBodyParam('owner');

        if (is_null($ownerId) || is_null($owner)) {
            throw new HttpException(400, 'Request has no `ownerId` and `owner` params');
        }

        if (!in_array($owner, array_keys(self::OWNER_CLASSES))) {
            throw new HttpException(400, 'Invalid `owner` param value');
        }

        $addressOwner = $this->getAddressOwner($owner, $ownerId);

        /**
         * @var Address $entity
         */
        $address = new Address();
        $address->load(Yii::$app->getRequest()->getBodyParams(), '');
        $address->save();

        try{
            $addressOwner->bindAddress($address->id);
        } catch (\Exception $e) {
            $address->delete();
            throw $e;
        }

        $response = Yii::$app->response;
        $response->setStatusCode(201);
        $response->headers->add('id', $address->id);

        return true;
    }

    /**
     * @param string $owner
     * @param int $ownerId
     * @return IBaseCrudModel|null
     */
    private function getAddressOwner(string $owner, int $ownerId): ?IBaseCrudModel
    {
        $ownerClass =  self::OWNER_CLASSES[$owner] ?? null;

        if ($ownerClass) {
            return $ownerClass::findOne($ownerId);
        }

        return null;
    }
}