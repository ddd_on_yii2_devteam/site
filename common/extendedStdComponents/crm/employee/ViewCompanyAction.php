<?php

namespace common\extendedStdComponents\crm\employee;

use commonprj\components\crm\entities\employee\Employee;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class Employee * @package api\controllers
 */
class ViewCompanyAction extends BaseAction
{

    /**
     * @param int $id
     * @return \commonprj\components\crm\entities\company\Company|\commonprj\components\crm\entities\employee\AbstractCompany|null
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id) 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Employee $entity
         */
        $entity = $this->findModel($id);

        return $entity->getCompany();
    }

}