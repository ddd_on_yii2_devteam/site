<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29.03.2018
 * Time: 13:32
 */

namespace common\extendedStdComponents\crm\employee;

use commonprj\components\crm\entities\employee\Employee;
use commonprj\extendedStdComponents\BaseAction;
use Yii;
use yii\web\HttpException;

class CreateEmployeeAndBindCompanyAction extends BaseAction
{
    /**
     * @return bool
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $companyId = Yii::$app->getRequest()->getBodyParam('companyId');

        if (is_null($companyId)) {
            throw new HttpException(400, 'Request has no `companyId` param');
        }


        /**
         * @var Employee $employee
         */
        $employee = new Employee();
        $employee->load(Yii::$app->getRequest()->getBodyParams(), '');
        $employee->save();

        try{
            $employee->bindCompany($companyId);
        } catch (\Exception $e) {
            $employee->delete();
            throw $e;
        }

        $response = Yii::$app->response;
        $response->setStatusCode(201);
        $response->headers->add('id', $employee->id);

        return true;
    }
}