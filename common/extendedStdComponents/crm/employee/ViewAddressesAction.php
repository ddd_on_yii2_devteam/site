<?php

namespace common\extendedStdComponents\crm\employee;

use commonprj\components\crm\entities\employee\Employee;
use commonprj\extendedStdComponents\BaseAction;
use Yii;

/**
 * Class Employee * @package api\controllers
 */
class ViewAddressesAction extends BaseAction
{

    /**
     * @param int $id
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id): array 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Employee $entity
         */
        $entity = $this->findModel($id);
        $typeId = Yii::$app->getRequest()->getQueryParam('typeId');
        return $entity->getAddresses($typeId) ?? [];
    }
}