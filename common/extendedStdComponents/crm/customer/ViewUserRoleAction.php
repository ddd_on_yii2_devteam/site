<?php

namespace common\extendedStdComponents\crm\customer;

use commonprj\components\crm\entities\customer\Customer;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class Customer * @package api\controllers
 */
class ViewUserRoleAction extends BaseAction
{

    /**
     * @param int $id
     * @return \commonprj\components\crm\entities\userRole\UserRole|null
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id) 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Customer $entity
         */
        $entity = $this->findModel($id);

        return $entity->getUserRole();
    }

}