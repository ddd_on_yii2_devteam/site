<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 14.02.2018
 * Time: 13:57
 */

namespace common\extendedStdComponents\crm\directories;

use common\extendedStdComponents\DirectoriesAction;
use commonprj\components\core\entities\property\Property;
use commonprj\components\crm\entities\address\Address;

/**
 * Class ViewPropertyDirectoriesAction
 * @package common\extendedStdComponents\property
 */
class ViewDirectoriesAction extends DirectoriesAction
{

    /**
     * @var array
     */
    public $directories = [
        'languages'        => [self::class, 'getLanguages'],
        'countries'        => [self::class, 'getCountries'],
        'currencies'       => [self::class, 'getCurrencies'],
        'typesOfCompanies' => [self::class, 'getTypesOfCompanies'],
        'typesOfAddresses' => [self::class, 'getTypesOfAddresses'],
    ];

    /**
     * @return array
     */
    protected function getLanguages()
    {
        $response = Property::findAll(['sysname__eq' => 'language']);
        $propertyData = $response['items'][0] ?? null;

        $property = (new Property($propertyData));

        $listData = $property->getListItems();

        $result = [];

        foreach ($listData as $listDatum) {
            list($language, $country) = explode('-', $listDatum['item']);
            $listDatum['language'] = $language;
            $result[] = $listDatum;
        }

        return $result;
    }

    /**
     * @return array
     */
    protected function getTypesOfCompanies()
    {
        $result = [
            ['item' => '1', 'label' => 'manufacturer'],
            ['item' => '2', 'label' => 'seller'],
        ];

        return $result;
    }

    /**
     * @return array
     */
    protected function getTypesOfAddresses()
    {
        $result = [];
        foreach ((new Address())->types as $key => $type) {
            $result[] = [
                'item'  => $key,
                'label' => $type,
            ];
        }

        return $result;
    }

    /**
     * @return array
     */
    protected function getCountries()
    {
        $response = Property::findAll(['sysname__eq' => 'country']);
        $propertyData = $response['items'][0] ?? null;

        $property = (new Property($propertyData));

        $listData = $property->getListItems();
        $listData = array_values($listData);

        return $listData;
    }

    /**
     * @return array
     */
    protected function getCurrencies()
    {
        $response = Property::findAll(['sysname__eq' => 'currency']);
        $propertyData = $response['items'][0] ?? null;

        $property = (new Property($propertyData));

        $listData = $property->getListItems();
        $listData = array_values($listData);

        return $listData;
    }
}