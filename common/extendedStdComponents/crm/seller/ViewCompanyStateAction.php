<?php

namespace common\extendedStdComponents\crm\seller;

use commonprj\components\crm\entities\seller\Seller;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class Seller * @package api\controllers
 */
class ViewCompanyStateAction extends BaseAction
{

    /**
     * @param int $id
     * @return \commonprj\components\crm\entities\companyState\CompanyState
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id) 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Seller $entity
         */
        $entity = $this->findModel($id);

        return $entity->getCompanyState();
    }

}