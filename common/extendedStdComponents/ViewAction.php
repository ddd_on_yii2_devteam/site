<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 29.06.2016
 */

namespace common\extendedStdComponents;

use commonprj\extendedStdComponents\BaseCrudModel;
use commonprj\extendedStdComponents\BaseAction;
use Yii;

/**
 * ViewAction implements the API endpoint for returning the detailed information about a model.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ViewAction extends BaseAction
{
    /**
     * @param $id
     * @return BaseCrudModel
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {

        $model = $this->findModel($id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        return $model;
    }
}
