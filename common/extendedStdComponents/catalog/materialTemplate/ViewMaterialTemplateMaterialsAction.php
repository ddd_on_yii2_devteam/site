<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28.02.2018
 * Time: 17:32
 */

namespace common\extendedStdComponents\catalog\materialTemplate;

use commonprj\components\catalog\entities\materialTemplate\MaterialTemplate;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ViewMaterialTemplateMaterialsAction
 * @package common\extendedStdComponents\materialTempla
 */
class ViewMaterialTemplateMaterialsAction extends BaseAction
{
    /**
     * @param $id
     * @return array
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        /**
         * @var MaterialTemplate $entity
         */
        $entity = $this->findModel($id);

        return $entity->getMaterials();
    }
}