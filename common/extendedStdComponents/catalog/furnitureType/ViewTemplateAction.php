<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11.04.2018
 * Time: 12:54
 */

namespace common\extendedStdComponents\catalog\furnitureType;

use common\extendedStdComponents\AbstractBackendAction;
use commonprj\components\core\entities\property\Property;
use Yii;
use yii\web\HttpException;

/**
 * Class ViewTemplateAction
 * @package common\extendedStdComponents\core\property
 */
class ViewTemplateAction extends AbstractBackendAction
{
    const PROPERTY_SYSNAME = 'furnitureType';
    const ELEMENT_CLASS_NAME = 'catalog\ProductModel';

    /**
     * @param $id
     * @return \commonprj\components\core\entities\template\Template|null
     * @throws HttpException
     */
    public function run($id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $isInHierarchy = (Yii::$app->getRequest()->getQueryParam('isInHierarchy') == 'true') ? true : false;
        $multiplicityId = Yii::$app->getRequest()->getQueryParam('multiplicityId');

        /**
         * @var Property $property
         */
        $property = Property::findOne($this->getPropertyId());

        return $property->getTemplate($this->getElementClassId($property), $id, $multiplicityId, $isInHierarchy);
    }

    /**
     * @param Property $property
     * @return int
     * @throws HttpException
     */
    private function getElementClassId(Property $property): int
    {
        $elementClasses = $property->getElementClasses();

        foreach ($elementClasses as $elementClass) {
            if ($elementClass['name'] == self::ELEMENT_CLASS_NAME) {
                return $elementClass['id'];
            }
        }

        throw new HttpException(500, 'Не удалось найти нужный ElementClass');
    }
}