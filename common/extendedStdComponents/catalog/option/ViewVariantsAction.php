<?php

namespace common\extendedStdComponents\catalog\option;

use commonprj\components\catalog\entities\option\Option;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class Option * @package api\controllers
 */
class ViewVariantsAction extends BaseAction
{

    /**
     * @param int $id
     * @return array
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id): array 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Option $entity
         */
        $entity = $this->findModel($id);

        return $entity->getVariants() ?? [];
    }

}