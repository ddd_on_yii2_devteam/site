<?php

namespace common\extendedStdComponents\catalog\option;

use commonprj\components\catalog\entities\option\Option;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class Option * @package api\controllers
 */
class CreateRelationOption2PropertyAction extends BaseAction
{

    /**
     * @param int $id
     * @param int $elementId
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id, int $elementId) 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Option $entity
         */
        $entity = $this->findModel($id);

        return $entity->bindProperty($elementId);
    }

}