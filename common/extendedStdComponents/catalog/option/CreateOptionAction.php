<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 02.04.2018
 * Time: 17:41
 */

namespace common\extendedStdComponents\catalog\option;

use commonprj\components\catalog\entities\option\Option;
use commonprj\extendedStdComponents\BaseAction;
use Yii;
use yii\web\HttpException;

class CreateOptionAction extends BaseAction
{
    protected const REQUIRED_PARAMS = [
        'propertyId',
        'materialGroupId',
    ];

    protected const TYPE_OF_OPTIONS_HARDCODED = [
        'Property',
        'Material',
        'PriceCategory',
    ];

    /**
     * @return mixed
     * @throws HttpException
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $requiredParams = Yii::$app->getRequest()->getBodyParams();

        // проверка существования и валидного значения typeOfOption
        if (is_null($requiredParams['properties']['typeOfOption']) || !in_array($requiredParams['properties']['typeOfOption'], self::TYPE_OF_OPTIONS_HARDCODED)) {
            throw new HttpException(400, "Request has invalid `typeOfOption` param");
        }

        // Для typeOfOption Property
        if ($requiredParams['properties']['typeOfOption'] == 'Property' && is_null($requiredParams['properties']['propertyId'])) {
            throw new HttpException(400, "Request mast have `propertyId`");
        } else {
            if (in_array('materialGroupId', array_keys($requiredParams))) {
                unset($requiredParams['properties']['materialGroupId']);
            }
        }

        // Для НЕ typeOfOption Property
        if ($requiredParams['properties']['typeOfOption'] != 'Property' && is_null($requiredParams['properties']['materialGroupId'])) {
            throw new HttpException(400, "Request mast have `propertyId`");
        } else {
            if (in_array('propertyId', array_keys($requiredParams))) {
                unset($requiredParams['properties']['propertyId']);
            }
        }


        /**
         * @var Option $option
         */
        $option = new Option();
        $option->load($requiredParams, '');
        $option->save();

        $response = Yii::$app->response;
        $response->setStatusCode(201);
        $response->headers->add('id', $option->id);

        return true;
    }


}