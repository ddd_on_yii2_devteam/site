<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28.02.2018
 * Time: 14:06
 */

namespace common\extendedStdComponents\catalog\materialGroup;

use commonprj\components\catalog\entities\materialGroup\MaterialGroup;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ViewMaterialGroupMaterialsAction
 * @package common\extendedStdComponents\materialGroup
 */
class ViewMaterialGroupMaterialsAction extends BaseAction
{
    /**
     * @param $id
     * @return array|\commonprj\components\catalog\entities\material\Material[]
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     */
    public function run($id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var MaterialGroup $entity
         */
        $entity = $this->findModel($id);

        return $entity->getMaterials();
    }
}