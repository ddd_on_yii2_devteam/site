<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.04.2018
 * Time: 14:08
 */

namespace common\extendedStdComponents\catalog\productMaterial;

use commonprj\components\crm\entities\manufacturer\Manufacturer;
use commonprj\extendedStdComponents\BaseAction;
use Yii;

class ViewWithExtendedGroupingAction extends BaseAction
{
    /**
     * @var array
     */
    private $productMaterials;

    /**
     * @var array
     */
    private $priceCategories;

    /**
     * @var array
     */
    private $materialCollections;

    /**
     * @return array
     */
    public function run()
    {
        $storeService = Yii::$app->storeService;
        $manufacturer = Manufacturer::findOne($storeService->getCurrentCompanyId());
        $manufacturer = Manufacturer::findOne(86);

        $this->productMaterials = $manufacturer->getProductMaterials();
        $this->priceCategories = $this->setIndexes($manufacturer->getPriceCategories());
        $this->materialCollections = $this->setIndexes($manufacturer->getMaterialCollections());

        foreach ($this->productMaterials as $productMaterial) {

            if ($productMaterial->materialCollectionId) {
                $this->materialCollections[$productMaterial->materialCollectionId]->productMaterials[] = $productMaterial;
                $this->priceCategories[$productMaterial->priceCategoryId]->materialCollections[$productMaterial->materialCollectionId] = $this->materialCollections[$productMaterial->materialCollectionId];
            } else {
                $this->priceCategories[$productMaterial->priceCategoryId]->productMaterials[] = $productMaterial;
            }

        }

        foreach ($this->priceCategories as &$priceCategory) {
            if ($priceCategory->materialCollections) {
                $priceCategory->materialCollections = array_values($priceCategory->materialCollections);
            } else {
                $priceCategory->materialCollections = null;
            }
        }

        return array_values($this->priceCategories);

    }

    /**
     * @param $array
     * @return array
     */
    private function setIndexes($array)
    {
        $result = [];

        foreach ($array as $value) {
            $result[$value['id']] = $value;
        }

        return $result;
    }
}