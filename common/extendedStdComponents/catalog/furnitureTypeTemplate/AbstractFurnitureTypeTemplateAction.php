<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05.04.2018
 * Time: 14:27
 */

namespace common\extendedStdComponents\catalog\furnitureTypeTemplate;

use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\components\core\entities\property\Property;
use commonprj\extendedStdComponents\BaseAction;
use commonprj\extendedStdComponents\BaseServiceRepository;

class AbstractFurnitureTypeTemplateAction extends BaseAction
{
    private const ELEMENT_CLASS_NAME = 'catalog\ProductModel';
    private const PROPERTY_SYSNAME = 'furnitureType';

    /**
     * @return int|null
     * @throws \yii\web\HttpException
     */
    protected function getPropertyId()
    {
        return BaseServiceRepository::getIdByEntityClassAndCondition(Property::class, ['sysname__eq' => self::PROPERTY_SYSNAME]);
    }

    /**
     * @return int|null
     * @throws \yii\web\HttpException
     */
    protected function getElementClassId()
    {
        return BaseServiceRepository::getIdByEntityClassAndCondition(ElementClass::class, ['name__eq' => self::ELEMENT_CLASS_NAME]);
    }
}