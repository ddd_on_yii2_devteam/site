<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 04.04.2018
 * Time: 18:45
 */

namespace common\extendedStdComponents\catalog\furnitureTypeTemplate;

use commonprj\components\core\entities\propertyVariant\PropertyVariantServiceRepository;
use commonprj\components\core\entities\template\Template;
use Yii;
use yii\web\HttpException;

class CreateFurnitureTypeTemplateAction extends AbstractFurnitureTypeTemplateAction
{
    /**
     * @return mixed
     * @throws HttpException
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $furnitureTypePropertyId = $this->getPropertyId();
        $furnitureTypeId = Yii::$app->getRequest()->getBodyParam('furnitureTypeId');

        // find or create propertyVariantId
        $propertyVariantId = PropertyVariantServiceRepository::getPropertyVariantId($furnitureTypePropertyId, $furnitureTypeId);

        /**
         * @var Template $template
         */
        $template = new $this->modelClass();
        $template->load([
            'propertyVariantId' => $propertyVariantId,
            'propertyIds'       => Yii::$app->getRequest()->getBodyParam('propertyIds') ?? [],
            'elementClassId'    => $this->getElementClassId(),
        ], '');

        $template->save();

        $response = Yii::$app->response;
        $response->setStatusCode(201);
        $response->headers->add('id', $template->id);
    }
}