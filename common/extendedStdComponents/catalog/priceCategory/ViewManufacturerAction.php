<?php

namespace common\extendedStdComponents\catalog\priceCategory;

use commonprj\components\catalog\entities\priceCategory\PriceCategory;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class PriceCategory * @package api\controllers
 */
class ViewManufacturerAction extends BaseAction
{

    /**
     * @param int $id
     * @return \commonprj\components\crm\entities\manufacturer\Manufacturer
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id) 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var PriceCategory $entity
         */
        $entity = $this->findModel($id);

        return $entity->getManufacturer();
    }

}