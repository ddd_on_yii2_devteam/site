<?php

namespace common\extendedStdComponents\catalog\material;

use commonprj\components\catalog\entities\material\Material;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class Material * @package api\controllers
 */
class ViewMaterialGroupAction extends BaseAction
{

    /**
     * @param int $id
     * @return array|\commonprj\components\catalog\entities\materialGroup\MaterialGroup|null
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id) 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Material $entity
         */
        $entity = $this->findModel($id);

        return $entity->getMaterialGroups();
    }

}