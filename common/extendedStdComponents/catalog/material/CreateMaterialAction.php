<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 21.03.2018
 * Time: 16:13
 */

namespace common\extendedStdComponents\catalog\material;

use commonprj\components\catalog\entities\material\Material;
use commonprj\extendedStdComponents\BaseAction;
use commonprj\extendedStdComponents\BaseCrudModel;
use Yii;

/**
 * Class CreateMaterialAction
 * @package common\extendedStdComponents\material
 */
class CreateMaterialAction extends BaseAction
{
    /**
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $params = Yii::$app->getRequest()->getBodyParams();

        if (isset($params['hierarchyParentId'])) {
            $hierarchyParentId = $params['hierarchyParentId'];
            unset($params['hierarchyParentId']);
            Yii::$app->getRequest()->setBodyParams($params);
        }

        /** @var Material $model */
        $model = new $this->modelClass();
        $model->setAttributes(Yii::$app->getRequest()->getBodyParams(), false);

        $result = $model->save();

        if (isset($hierarchyParentId)) {
            $parent = Material::findOne($hierarchyParentId);
            $parent->bindHierarchyChild($model->id);
        }

        $response = Yii::$app->response;
        $response->setStatusCode(201);
        $response->headers->add('id', $model->id);

        return $result;
    }

}