<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 20.03.2018
 * Time: 11:05
 */

namespace common\extendedStdComponents\catalog\material;

use commonprj\components\catalog\entities\material\Material;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class DeleteRelationMaterial2HierarchyChildAction
 * @package common\extendedStdComponents\material
 */
class DeleteRelationMaterial2HierarchyChildAction extends BaseAction
{
    /**
     * @param int $id
     * @param int $elementId
     * @return bool
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id, int $elementId)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Material $entity
         */
        $entity = $this->findModel($id);

        return $entity->unbindHierarchyChild($elementId);
    }
}