<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 26.03.2018
 * Time: 17:06
 */

namespace common\extendedStdComponents\catalog\variant;

use commonprj\components\catalog\entities\option\Option;
use commonprj\components\catalog\entities\variant\Variant;
use commonprj\extendedStdComponents\BaseAction;
use Yii;
use yii\web\HttpException;

class CreateVariantAndBindOptionAction extends BaseAction
{

    /**
     * @return bool
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Variant $entity
        //         */
        $entity = new Variant();
        $attributes = Yii::$app->getRequest()->getBodyParams();
        $optionId = Yii::$app->getRequest()->getBodyParam('optionId') ?? null;

        if (!$optionId) {
            throw new HttpException(500, "Нет обязательного параметра $optionId");
        }

        // findOne() throws 404 if option not exist
        $option = Option::findOne($optionId);

        $entity = new Variant();
        $entity->load($attributes, '');
        $result = $entity->save();

        $response = Yii::$app->response;
        $response->setStatusCode(201);
        $response->headers->add('id', $entity->id);

        $result = $entity->bindOption($optionId);

        return $result;
    }
}