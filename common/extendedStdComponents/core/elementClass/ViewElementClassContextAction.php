<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.02.2018
 * Time: 19:25
 */

namespace common\extendedStdComponents\core\elementClass;

use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\extendedStdComponents\BaseAction;

class ViewElementClassContextAction extends BaseAction
{
    /**
     * @param $id
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        /**
         * @var ElementClass $entity
         */
        $entity = $this->findModel($id);

        return $entity->getContext();
    }
}