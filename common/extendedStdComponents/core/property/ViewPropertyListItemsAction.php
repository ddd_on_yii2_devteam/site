<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.02.2018
 * Time: 19:18
 */

namespace common\extendedStdComponents\core\property;

use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ViewPropertyListItemsAction
 * @package common\extendedStdComponents\property
 */
class ViewPropertyListItemsAction extends BaseAction
{
    /**
     * @param $id
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        /**
         * @var Property $entity
         */
        $entity = $this->findModel($id);

        return array_values($entity->getListItems());
    }
}