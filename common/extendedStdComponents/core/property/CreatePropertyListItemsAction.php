<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 27.02.2018
 * Time: 18:37
 */

namespace common\extendedStdComponents\core\property;

use commonprj\extendedStdComponents\BaseAction;
use commonprj\extendedStdComponents\BaseDBRepository;
use Yii;
use yii\db\ActiveRecord;
use yii\web\ServerErrorHttpException;

class CreatePropertyListItemsAction extends BaseAction
{
    /**
     * @param $id
     * @return mixed
     * @throws ServerErrorHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /** @var BaseCrudModel $model */
        $model = $this->findModel($id);
        $attributes = Yii::$app->getRequest()->getBodyParams();

        $listItems = [];

        foreach ((array)$attributes['items'] as $listItem) {
            $listItems[$listItem['item']] = $listItem['label'];
        }

        $result = $model->setListItems($listItems);

        if (!$result && !$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $result;
    }
}