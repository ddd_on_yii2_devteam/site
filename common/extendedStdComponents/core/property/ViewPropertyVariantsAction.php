<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.02.2018
 * Time: 19:11
 */

namespace common\extendedStdComponents\core\property;

use commonprj\components\core\entities\property\Property;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ViewPropertyVariantsAction
 * @package common\extendedStdComponents\property
 */
class ViewPropertyVariantsAction extends BaseAction
{
    /**
     * @param $id
     * @return array|\commonprj\components\core\entities\propertyVariant\PropertyVariant[]
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        /**
         * @var Property $entity
         */
        $entity = $this->findModel($id);

        return $entity->getPropertyVariants();
    }
}