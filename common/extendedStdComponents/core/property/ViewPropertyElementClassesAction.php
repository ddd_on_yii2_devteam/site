<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.02.2018
 * Time: 18:54
 */

namespace common\extendedStdComponents\core\property;

use commonprj\components\core\entities\property\Property;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ViewPropertyElementClassesAction
 * @package common\extendedStdComponents\property
 */
class ViewPropertyElementClassesAction extends BaseAction
{
    /**
     * @param $id
     * @return \commonprj\components\core\entities\elementClass\ElementClass[]
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        /**
         * @var Property $entity
         */
        $entity = $this->findModel($id);

        return $entity->getElementClasses();
    }
}
