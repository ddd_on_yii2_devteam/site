<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 14.02.2018
 * Time: 13:57
 */

namespace common\extendedStdComponents\core\property;

use common\extendedStdComponents\DirectoriesAction;
use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\components\core\entities\propertyGroup\PropertyGroup;
use commonprj\components\core\entities\propertyType\PropertyType;
use commonprj\components\core\entities\propertyUnit\PropertyUnit;


/**
 * Class ViewPropertyDirectoriesAction
 * @package common\extendedStdComponents\property
 */
class ViewPropertyDirectoriesAction extends DirectoriesAction
{

    public $directories = [
        'propertyUnits'  => PropertyUnit::class,
        'propertyGroups' => PropertyGroup::class,
        'propertyTypes'  => PropertyType::class,
        'elementClasses' => ElementClass::class,
    ];

}