<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 05.07.2016
 */

namespace common\extendedStdComponents;

use commonprj\components\core\entities\common\variant\Variant;
use commonprj\extendedStdComponents\BaseCrudModel;
use commonprj\extendedStdComponents\BaseAction;
use Yii;
use yii\web\HttpException;
use yii\web\ServerErrorHttpException;

/**
 * DeleteAction implements the API endpoint for deleting a model.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DeleteAction extends BaseAction
{
    /**
     * @param $id
     * @return bool|BaseCrudModel
     * @throws HttpException
     * @throws ServerErrorHttpException
     */
    public function run($id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /** @var BaseCrudModel $entity */
        $entity = new $this->modelClass();
        $entity->id = $id;

        $result = $entity->delete();

        if ($result === false) {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }

        return $result;
    }
}
