<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 04.04.2018
 * Time: 16:11
 */

namespace common\extendedStdComponents;

use commonprj\components\core\entities\property\Property;
use commonprj\extendedStdComponents\BaseAction;
use Yii;

/**
 * Class AbstractIndexTreeAction
 * @package common\extendedStdComponents
 */
class AbstractIndexTreeAction extends AbstractBackendAction
{
    /**
     * @return mixed
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $queryParams = [];

        $viewTree = Yii::$app->getRequest()->getQueryParam('view');

        if (!is_null($viewTree) && $viewTree == 'tree') {
            $queryParams['view'] = 'tree';
        }

        $queryParams['propertyId'] = $this->getPropertyId();

        return $this->modelClass::findAll($queryParams);
    }

}