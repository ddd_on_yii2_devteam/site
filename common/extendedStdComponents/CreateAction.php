<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 04.07.2016
 */

namespace common\extendedStdComponents;

use commonprj\components\core\entities\variant\Variant;
use commonprj\extendedStdComponents\BaseCrudModel;
use commonprj\extendedStdComponents\BaseDBRepository;
use commonprj\extendedStdComponents\BaseAction;
use Yii;
use yii\base\Model;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;
use yii\db\ActiveRecord;

/**
 * CreateAction implements the API endpoint for creating a new model from the given data.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CreateAction extends BaseAction
{
    /**
     * @var string the scenario to be assigned to the new model before it is validated and saved.
     */
    public $scenario = Model::SCENARIO_DEFAULT;

    /**
     * @return Variant|BaseCrudModel
     * @throws ServerErrorHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /** @var BaseCrudModel $model */
        $model = new $this->modelClass();
        if (is_subclass_of($model, ActiveRecord::class)) {
            $model->setAttributes(BaseDBRepository::arrayKeysCamelCase2Underscore(Yii::$app->getRequest()->getBodyParams()), false);
        } else {
            $model->setAttributes(Yii::$app->getRequest()->getBodyParams(), false);
        }

        $result = $model->save();
        $response = Yii::$app->response;

        $response->setStatusCode(201);
        $response->headers->add('id', $model->id);

        return $result;
    }
}
